#!/bin/bash
# Generate an Elliptic Curve key pair using the `secp256k1` curve (used in Bitcoin) and store the keys in hex format.
# Requires openssl

if [ $# -ne 1 ]; then
	echo "Usage: $(basename $0) path_and_filename_for_the_generated_keys"
	exit 0;
fi

PATH_WITH_FILENAME=$1
KEY_FILES_EXTENSION="txt"

TMP_PRIVATE_FILE='./private.pem'
openssl ecparam -genkey -name secp256k1 -out ./private.pem
# Convertion from DER to hex: https://bitcoin.stackexchange.com/a/11843/
openssl ec -in ./private.pem -outform der 2>/dev/null | tail -c +8 | head -c 32 | xxd -p -c 32 > ${PATH_WITH_FILENAME}-private.${KEY_FILES_EXTENSION}
openssl ec -in ./private.pem -pubout -outform der 2>/dev/null | tail -c 65 | xxd -p -c 65 > ${PATH_WITH_FILENAME}-public.${KEY_FILES_EXTENSION}
rm -rf ${TMP_PRIVATE_FILE}
echo "Done, keys where saved in ${PATH_WITH_FILENAME}-{private,public}.${KEY_FILES_EXTENSION}"