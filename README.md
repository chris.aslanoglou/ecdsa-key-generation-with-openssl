## Generate secp256k1 EC key pair using OpenSSL

### Requirements
OpenSSL to be installed and accessible from PATH.

### Usage
`./generate-ec-pair-in-hex-format.sh path_and_filename_for_the_generated_keys`

### Information
Instructions are based from this OpenSSL guide
https://wiki.openssl.org/index.php/Command_Line_Elliptic_Curve_Operations

Steps took to generate keys (default output format is PEM):
1) Used this `openssl ecparam -genkey -name secp256k1 -out private.pem` to generate the EC private key in PEM format.
2) To generate in DER format: `openssl ec -in ./private.pem -outform der -out ./private.der`.
3) To generate the public key in PEM format: `openssl ec -in ./private.pem -pubout -out ./public.pem`.
4) To generate the public key in DER format: `openssl ec -in ./private.pem -pubout -outform der -out ./public.der`.

In order to get the keys in hex format (as in Blockstack's profile) use getpub2DER and getpriv2DER, adapted from:
https://bitcoin.stackexchange.com/a/11843/82568d

or use `openssl asn1parse -in ./private.der -inform DER` and copy the OCTET STRING field, which is your key in hex
(source of the above command https://wiki.openssl.org/index.php/DER).
Respectively, for the public key: `openssl asn1parse -in ./public.der -inform DER -dump`, which requires a bit of
processing to extract the uncompressed private key.

Let's break down what this `openssl ec -in tmp/data.pem -outform DER|tail -c +8|head -c 32|xxd -p -c 32` does:
1) `openssl ec -in tmp/data.pem -outform DER`, same as (2) above.
2) `tail -c +8` => skips the first (due to "+") 8 bytes (due to "c"), which should be the header of the DER format
3) `head -c 32` => returns the first 32 bytes which is the private key length
4) `xxd -p -c 32` => does a hex dump, since the private key is in binary format.
        -p is for having the output in plain hexdump style
        -c 32 => for having the 32 characters in one line, default is 16.
